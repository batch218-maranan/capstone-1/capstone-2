const User = require("../models/user.js"); // returns objects, literal and function, not the whole document.
const Product = require ("../models/product.js")
const bcrypt = require("bcrypt"); // bcrypt - package for password hashing
const auth = require("../auth.js")


// Create product
module.exports.addProduct = (data) => {
	console.log(data.isAdmin)

	if (data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		});

		return newProduct.save().then((newProduct, error) => {
			if (error) {
				return error
			}
			return newProduct
		})
	};

	//  If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve('User must be an ADMIN to add a product.')

	return message.then((value) => {
		return {value}
	})
};

// Get all active products
module.exports.getAllActiveProducts = () => {
	return Product.find({isActive:true}).then(result => {
		return result
	})
}

// GET specific product
module.exports.getProduct = (productId) => {
	return Product.findById(productId).then(result => {
		return result;
	})
}

// Update product
module.exports.updateProduct = (productId, newData) => {
	if (newData.isAdmin == true){
		return Product.findByIdAndUpdate(productId, 
		{
			name : newData.product.name,
			description: newData.product.description,
			price: newData.product.price
		}).then((updatedProduct, error) => {
			if (error) {
				return false
			} return true
			}) 
		} else {
				let message = Promise.resolve('User must be ADMIN to access this');
				return message.then((value) => {return value})
	}
}

// Archive Product
module.exports.archiveProduct = (productId) => {
	return Product.findByIdAndUpdate(productId, {
		isActive: false
	})
	.then((archivedProduct, error) => {
		if(error){
			return false
		} 

		return {
			message: "Product archived successfully!"
		}
	})
}