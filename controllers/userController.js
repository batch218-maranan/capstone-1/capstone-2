const User = require("../models/user.js"); // returns objects, literal and function, not the whole document.
const Product = require ("../models/product.js")
const bcrypt = require("bcrypt"); // bcrypt - package for password hashing
const auth = require("../auth.js")


// Register User
module.exports.registerUser = (reqBody) => {
	
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo,
		isAdmin: reqBody.isAdmin
	})

	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})
}

// Login User
module.exports.loginUser = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result => {
		if (result == null) {
			return false;
		} else {
			// compareSync is bcrypt function to compare a hashed password to unhashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				// if password does not match
				return false;
				// return "Incorrect password"
			}
		}
	})
}

// Create Order
module.exports.createOrder = (data) => {

  if (typeof data.isAdmin === true && data.isAdmin) {
    return "Cannot checkout item";
  } else {
    let newProduct = new Product({
      name: data.product.name,
      description: data.product.description,
      price: data.product.price
    });

    return newProduct.save().then((newProduct, error) => {
      if (error) {
        return error;
      }
      return newProduct;
    });
  }
};




// Retrieve User details
module.exports.getDetails = (userId) => {
	return User.findById(userId).then(result => {
		return result;
	})
}

