// Schema

const mongoose = require("mongoose");
mongoose.set('strictQuery', true)


const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required : [true, "Product name is required"]
    },
    description:{
        type: String,
        required: [true, "Description is required"]
    },
    price: {
        type: Number,
        required: [true, "Price is required"]
    },
    isActive:{
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        // The "new Date()" expression instantiates a new "date" that the current date and time whenever a course is created in our database
        default: new Date()
    },
     orders :[
        {
            orderId: {
                type: String,
                required: [true, "orderId is required"]
            }
        }
        ]
})

module.exports = mongoose.model("Product", productSchema)
