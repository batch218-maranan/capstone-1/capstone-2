// dependencies
const express = require("express");
const router = express.Router();
const User = require("../models/user.js");
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

// Register User
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// Login User
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})


// Create Order
router.post("/checkout", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.createOrder(data).then(
		resultFromController => res.send(resultFromController))
});



// Retrieve User details
router.get("/:userId", (req, res) => {
	userController.getDetails(req.params.userId).then(
		resultFromController => res.send(resultFromController))
})


module.exports = router;