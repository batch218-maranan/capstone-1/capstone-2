const express = require("express"); // accessing package of express
const router = express.Router(); // use dot notation to access content of package

const productController = require("../controllers/productController.js");
const auth = require("../auth.js");

module.exports = router;

//  Create a single product
router.post("", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(
		resultFromController => res.send(resultFromController))
});


// GET all active products
router.get("/active", (req, res) => {
	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController))
})


// GET specific product by ID
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params.productId).then(
		resultFromController => res.send(resultFromController))
})

// Update product
router.patch("/:productId/update", auth.verify, (req, res) => {
	const newData = {
		product : req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	productController.updateProduct(req.params.productId, newData).then(
		resultFromController => {
			res.send(resultFromController)
		})
})

// Archive product
router.patch("/:productId/archive", auth.verify, (req, res) => {
	productController.archiveProduct(req.params.productId).then(resultFromController => {
		res.send(resultFromController)
	});
});
